# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import copy

import requests
from lxml import etree

from config import metadata_dict, project_ids

ns = {
    "datacite": "http://datacite.org/schema/kernel-3",
    "dc": "http://purl.org/dc/elements/1.1/",
    "marcRole": "http://id.loc.gov/vocabulary/relators/",
    "oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "tei": "http://www.tei-c.org/ns/1.0",
}


def get_metadata_from_oai_pmh(project_id):
    collected_metadata_dict = copy.deepcopy(
        metadata_dict
    )

    if (
        project_ids[
            "European Literary Text Collection (ELTeC)"
        ]
        == project_id
    ):
        base_url = "https://dev.textgridlab.org/1.0/tgoaipmh/oai"
    else:
        base_url = "https://textgridlab.org/1.0/tgoaipmh/oai"

    # request oai_dc
    params = {
        "verb": "ListRecords",  # "GetRecord",#ListSets"#,
        "metadataPrefix": "oai_dc",
        "set": f"project:{project_id}",
    }
    r = requests.get(base_url, params=params)
    root = etree.fromstring(r.content)
    string = etree.tostring(root).decode("UTF-8")

    rights = set(
        [
            element.text
            for element in root.xpath(
                "//dc:rights", namespaces=ns
            )
        ]
    )
    collected_metadata_dict["f_04_Lizenz"] = (
        rights
    )

    # request data_cite
    base_url = (
        "https://textgridlab.org/1.0/tgoaipmh/oai"
    )
    params = {
        "verb": "ListRecords",  # "GetRecord",#ListSets"#,
        "metadataPrefix": "oai_datacite",
        "set": f"project:{project_id}",
    }
    r = requests.get(base_url, params=params)
    root = etree.fromstring(r.content)
    string = etree.tostring(root).decode("UTF-8")

    dates_created = list(
        set(
            [
                element.text
                for element in root.xpath(
                    "//datacite:publicationYear",
                    namespaces=ns,
                )
            ]
        )
    )
    if len(dates_created) <= 1:
        collected_metadata_dict[
            "f_09_Erstellungsdatum"
        ] = str(dates_created).strip("[]'")
    else:
        collected_metadata_dict[
            "f_09_Erstellungsdatum"
        ] = f"{dates_created[0]} bis {dates_created[1]}"

    return collected_metadata_dict
