# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

from tgclients.aggregator import Aggregator
from tgclients.config import TextgridConfig
from tgclients.crud import TextgridCrud
from tgclients.search import (
    TextgridSearch,
    TextgridSearchRequest,
)
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    TextClassificationPipeline,
    pipeline,
)

from config import project_ids
from functions.iso import *


def get_project_languages(project_id: str) -> set:
    """Experimental function that extracts languages for for TextGrid projects based on transformers model."""
    # define endpoint
    if (
        project_ids[
            "European Literary Text Collection (ELTeC)"
        ]
        == project_id
    ):
        config = TextgridConfig(
            "https://dev.textgridlab.org"
        )
    else:
        config = TextgridConfig(
            "https://textgridlab.org"
        )
    aggregator = Aggregator(config=config)
    tgsearch = TextgridSearch(config=config)
    tgcrud = TextgridCrud(config=config)

    # load classifier
    model_name = "qanastek/51-languages-classifier"  # model_name = "papluca/xlm-roberta-base-language-detection"
    tokenizer = AutoTokenizer.from_pretrained(
        model_name
    )
    model = AutoModelForSequenceClassification.from_pretrained(
        model_name
    )
    classifier = TextClassificationPipeline(
        model=model, tokenizer=tokenizer
    )

    # get xml from from project
    results = tgsearch.search(
        filters=[
            f"project.id:{project_id}",
            "format:text/tg.edition+tg.aggregation+xml",
        ],
        start=0,
        limit=1000,
        path=True,
    )

    results_ = [
        {
            "edition_uri": result.object_value.generic.generated.textgrid_uri.value,
            "edition_agent": result.object_value.edition.agent[
                0
            ].value,
            "edition_title": result.object_value.generic.provided.title[
                0
            ],
        }
        for result in results.result
    ]

    # classify collection
    for i, dic in enumerate(results_):
        dic["text"] = aggregator.text(
            dic["edition_uri"]
        ).text
        dic["language_classification"] = (
            classifier(
                dic["text"],
                max_length=512,
                truncation=True,
            )
        )

    project_languages = set(
        [
            dic["language_classification"][0][
                "label"
            ]
            for dic in results_
        ]
    )

    project_languages = format_to_iso(
        project_languages
    )

    return project_languages
