# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import copy

from tgclients import (
    TextgridConfig,
    TextgridSearch,
)

from config import metadata_dict, project_ids
from functions.iso import format_to_iso
from functions.vlo_mapping import (
    apply_vlo_mapping,
)


def get_metadata_via_tgsearch(
    project_id: str,
) -> dict:
    """Function that queries TGsearch via tgclients and
    stores information in dict.
    """
    collected_metadata_dict = copy.deepcopy(metadata_dict)

    # define endpoint
    # add project id to list (or request to project_ids from vars)
    # if dealing with project from dev.textgrid
    if project_id in []:
        config = TextgridConfig("https://dev.textgridlab.org")
    else:
        config = TextgridConfig("https://textgridlab.org")

    # with data binding
    tgsearch = TextgridSearch(config=config)

    # # only xml file
    # tgsearch = TextgridSearchRequest(config=config)

    # f_03_Groessenbeschreibung / short description with hits and
    # mime type counts
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=["format"],
        start=0,
        limit=0,
    )
    mime_types = {
        i.value: i.count
        for i in results.facet_response.facet_group[0].facet
        if i.value
        in [
            "text/xml",
            "image/jpeg",
            "image/png",
            "image/tiff",
        ]
    }
    hits = results.hits
    mime_types_string = str(mime_types).strip("{}").replace("'", "`")

    collected_metadata_dict["f_03_Groessenbeschreibung"] = (
        f'Die Sammlung umfasst {mime_types["text/xml"]} TEI/XML-Dateien und {sum((mime_types.get("image/jpeg", 0), mime_types.get("image/png", 0), mime_types.get("image/tiff", 0)))} Bilder.'.replace(
            "1 Bilder", "1 Bild"
        ).replace(
            " und 0 Bilder", ""
        )
    )

    # f_04_Lizenz, f_05_Lizenz_URL / edition.license.value, edition.license.licenseUri
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=[
            "format",
            "item.rightsHolder",
            "edition.license.value",
            "edition.license.licenseUri",
        ],
        start=0,
        limit=0,
        facet_limit=999,
    )
    collected_metadata_dict["f_05_Lizenz_URL"] = {
        "uri": facet.value
        for facet_group in results.facet_response.facet_group
        for facet in facet_group.facet
        if facet_group.name == "edition.license.licenseUri"
    }
    collected_metadata_dict["f_05_Lizenz_URL"].update(
        {
            "text": " ".join(facet.value.replace("\n", "").split())
            for facet_group in results.facet_response.facet_group
            for facet in facet_group.facet
            if facet_group.name == "edition.license.value"
        }
    )
    # only keep licence uri since it is needed for registry
    collected_metadata_dict["f_05_Lizenz_URL"] = collected_metadata_dict[
        "f_05_Lizenz_URL"
    ]["uri"]

    # apply licence vocab needed for registry in field 4
    if "creativecommons" in collected_metadata_dict["f_05_Lizenz_URL"]:
        collected_metadata_dict["f_04_Lizenz"] = "Academic"
    else:
        collected_metadata_dict["f_04_Lizenz"] = "Unspecified"

    # f_07_Sprache / edition.language
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=["edition.language"],
        start=0,
        limit=0,
        facet_limit=999,
    )
    languages = [i.value for i in results.facet_response.facet_group[0].facet]
    collected_metadata_dict["f_07_Sprache"] = format_to_iso(languages)

    # f_08_Datentypisierung / VLO mapping based on file format
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=["format"],
        start=0,
        limit=0,
    )
    collected_metadata_dict["f_08_Datentypisierung"] = list(
        set(
            [
                apply_vlo_mapping(i.value)
                for i in results.facet_response.facet_group[0].facet
                if apply_vlo_mapping(i.value) is not None
            ]
        )
    )

    # f_11_abgedeckter_Zeitraum / Veröffentlichung
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=["work.dateOfCreation.value"],
        start=0,
        limit=0,
        facet_limit=999,
    )
    dates = sorted(
        [
            int(i.value)
            for i in results.facet_response.facet_group[0].facet
            if len(i.value) == 4
        ]
    )
    try:
        collected_metadata_dict["f_11_abgedeckter_Zeitraum"] = (
            f"{dates[0]} bis {dates[1]}"
        )
    except:
        collected_metadata_dict["f_11_abgedeckter_Zeitraum"] = dates

    return collected_metadata_dict

    ### UNUSED FIELDS ###

    # f_16_Genre / Genre
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=["work.genre"],
        start=0,
        limit=0,
        facet_limit=999,
    )
    collected_metadata_dict["f_16_Genre"] = {
        i.value: i.count for i in results.facet_response.facet_group[0].facet
    }

    # # f_17_fachliche_Zuordnung / Basisklassifikation
    # results = tgsearch.search(
    #     query="*",
    #     filters=["project.id:" + project_id],
    #     facet=["work.subject.id.value"],
    #     start=0,
    #     limit=0,
    #     facet_limit=999,
    # )
    # collected_metadata_dict["f_17_fachliche_Zuordnung"] = [
    #     i.value for i in results.facet_response.facet_group[0].facet
    # ]
    # print(collected_metadata_dict["f_01_Titel"])
    # print(collected_metadata_dict["f_17_fachliche_Zuordnung"])
    # input()

    # f_18_Schlagworte / list of gnd/viaf ids for authors
    results = tgsearch.search(
        query="*",
        filters=["project.id:" + project_id],
        facet=["work.agent.id"],
        start=0,
        limit=0,
        facet_limit=999,
    )
    author_ids = [
        i.value
        for i in results.facet_response.facet_group[0].facet
        if len(i.value) > 0
    ]
    author_ids_links = []
    for author_id in author_ids:
        if author_id.startswith("pnd") or author_id.startswith("gnd"):
            author_ids_links.append(
                str("https://d-nb.info/gnd/" + author_id.split(":")[1])
            )
        elif author_id.startswith("viaf"):
            author_ids_links.append(
                str("https://viaf.org/viaf/" + author_id.split(":")[1])
            )
        else:
            author_ids_links.append(author_id)

    collected_metadata_dict["f_18_Schlagworte"] = author_ids_links
