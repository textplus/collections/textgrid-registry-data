# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0


def apply_vlo_mapping(mime_type: str) -> str:
    mapping = {
        "application/postscript": None,
        "application/xml;derived=true": "text",
        "image/jpeg": "image",
        "image/png": "image",
        "image/svg+xml": "image",
        "image/tiff": "image",
        "text/markdown": "text",
        "text/tg.aggregation+xml": "collection",
        "text/tg.collection+tg.aggregation+xml": "collection",
        "text/tg.edition+tg.aggregation+xml": "collection",
        "text/tg.portalconfig+xml": "toolService",
        "text/tg.work+xml": None,
        "text/xsd+xml": "toolService",
        "text/xml": "text",
    }
    return mapping[mime_type]
