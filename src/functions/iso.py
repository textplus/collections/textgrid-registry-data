# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import csv
import os
from typing import Iterable

language_names = {
    "deu": {"de-DE"},
    "fra": {"French", "french", "fr-FR"},
    "ita": {"it-IT"},
    "spa": {"es-ES"},
}


def read_iso_dict() -> dict:
    with open(
        f"{os.path.dirname(os.path.abspath(__file__))}/../iso-639-3_Code_Tables_20230123/iso-639-3_20230123.tab",
        mode="r",
    ) as f:
        reader = csv.DictReader(f, delimiter="\t")
        iso_dict = {
            value: dic["Ref_Name"] for dic in reader for key, value in dic.items() if key == "Id"
        }
    return iso_dict


def string_to_iso(s: str, iso_dict: dict):
    if s in iso_dict.keys():
        return s
    else:
        for (
            key,
            language_names_set,
        ) in language_names.items():
            if s in language_names_set:
                s = key
            else:
                continue
        return s


def format_to_iso(iterable: Iterable):
    iso_dict = read_iso_dict()
    formated_languages = {
        string_to_iso(i, iso_dict=iso_dict): iso_dict[string_to_iso(i, iso_dict=iso_dict)]
        for i in iterable
        if len(i) > 0
    }
    return formated_languages


if __name__ == "__main__":
    iso_dict = read_iso_dict()
    print(string_to_iso("German", iso_dict=iso_dict))
