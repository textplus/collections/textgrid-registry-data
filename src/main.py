# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import copy
import csv
import os
from datetime import datetime

import requests
import yaml
from lxml import etree
from tgclients import (
    TextgridConfig,
    TextgridSearch,
    TextgridSearchRequest,
)
from yaml import BaseLoader

from config import metadata_dict, project_ids
from functions.iso import *
from functions.language_dectection import (
    get_project_languages,
)
from functions.oai_metadata import (
    get_metadata_from_oai_pmh,
)
from functions.tgsearch_metadata import (
    get_metadata_via_tgsearch,
)

tgsearch = TextgridSearch()

if __name__ == "__main__":

    # create collections dict with collection from TextGrid project and set specific metadata
    collections = dict()
    for project_id_key in project_ids:
        print(project_id_key)
        collections[project_id_key] = copy.deepcopy(metadata_dict)
        collections[project_id_key]["f_01_Titel"] = project_id_key
        collections[project_id_key][
            "f_21_PID"
        ] = f"https://textgridrep.org/project/{project_ids[project_id_key]}"

        # get collected_metadata from oai-pmh reqests
        collected_metadata_dict = get_metadata_from_oai_pmh(
            project_id=project_ids[project_id_key]
        )
        for (
            collected_metadata_name,
            value,
        ) in collected_metadata_dict.items():
            if value:
                collections[project_id_key][collected_metadata_name] = value

        # get metadata from tgsearch queries
        collected_metadata_dict = get_metadata_via_tgsearch(
            project_id=project_ids[project_id_key]
        )
        for (
            collected_metadata_name,
            value,
        ) in collected_metadata_dict.items():
            if value:
                collections[project_id_key][collected_metadata_name] = value

        # UNUSED
        # # detect language for project/collection
        # if collection_name in ["Digitale Bibliothek", "Neologie"]:
        #     if not collection_dict["f_07_Sprache"]:
        #         collection_dict["f_07_Sprache"] = dict()
        #     collection_dict["f_07_Sprache"].update(get_project_languages(project_id=project_ids[collection_name]))

    timestamp = "_" + str(datetime.now()).replace(":", "-")

    # read yaml input files
    yaml_dicts = {}
    for file in os.listdir(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "input",
        )
    ):
        with open(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                "input",
                file,
            ),
            mode="r",
            encoding="UTF-8",
        ) as file:
            yaml_dict = yaml.load(file, BaseLoader)
            yaml_dicts[yaml_dict["f_01_Titel"]] = yaml_dict

    # update all collection_dicts in collections with existing content from yaml input files
    for yaml_key, yaml_dict in yaml_dicts.items():
        for (
            collection_name,
            collection_dict,
        ) in collections.items():
            # write fields of yaml input file for all tg projects
            if yaml_key == "ALL_TG_PROJECTS":
                for (
                    yaml_field_key,
                    yaml_field_content,
                ) in yaml_dict.items():
                    if yaml_field_content != "ALL_TG_PROJECTS":
                        collection_dict[yaml_field_key] = yaml_field_content
            # write fields of yaml input files vor specific projects
            if yaml_key == collection_name:
                for (
                    yaml_field_key,
                    yaml_field_content,
                ) in yaml_dict.items():
                    collection_dict[yaml_field_key] = yaml_field_content

    # write YAML
    for (
        collection_name,
        collection_dict,
    ) in collections.items():
        with open(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                "output",
                f"{collection_name}.yml",
            ),
            mode="wt",
            encoding="utf-8",
        ) as file:
            yaml.dump(
                collection_dict,
                file,
                allow_unicode=True,
            )
