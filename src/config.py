# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

fields = [
    "f_01_Titel",
    "f_02_Beschreibung",
    "f_03_Groessenbeschreibung",
    "f_04_Lizenz",
    "f_05_Lizenz_URL",
    "f_06_Modalitaet",
    "f_07_Sprache",
    "f_08_Datentypisierung",
    "f_09_Erstellungsdatum",
    "f_10_Veroeffentlichungsdatum",
    "f_11_abgedeckter_Zeitraum",
    "f_12_Volltext_verfuegbar",
    # "Annotationslayer_vorhanden",
    "f_13_Annotationslayer",
    "f_13_2_Umfang_Annotationslayer",
    "f_14_Annotationen_manuell_oder_automatisch",
    "f_15_Kollektionstypen",
    "f_16_Genre",
    "f_17_fachliche_Zuordnung",
    "f_18_Schlagworte",
    "f_19_Periodizitaet",
    "f_20_Status_im_Datenlebenszyklus",
    "f_21_PID",
    "f_22_Zugangsinformation",
    "f_23_Hierarchie",
    "f_24_Bezug_zu_anderen_Sammlungen",
    "f_25_Liste_der_Dateien",
    "f_26_technische_Dokumentation",
    "f_27_Datenverantwortliche_Person_Institution",
    "f_28_Ansprechperson_Datenzugang",
    "f_29_Foerderer",
    "f_30_Titel_des_Projekts",
    "f_31_Foerderer_ID",
]

metadata_dict = dict()
for field in fields:
    metadata_dict[field] = None

project_ids = {
    "Digitale Bibliothek": "TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c",
    "European Literary Text Collection (ELTeC)": "TGPR-99d098e9-b60f-98fd-cda3-6448e07e619d",
    "Architrave": "TGPR-439779b8-db54-e35b-b8e5-590ae9bd74e9",
    "CoNSSA: Corpus of Novels of the Spanish Silver Age (version 2.0.0)": "TGPR-8b44ca41-6fa1-9b49-67b7-6374d97e29eb",
    "Neologie": "TGPR-60c6a362-f1f6-1c81-544b-53837e6c4fb1",
}
